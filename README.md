# Development Test #

Javascript / C#:  Use either Javascript or C# to write a code snippet to address the following problem:



**Problem 1**


Assume an array (javascript) or list (C#) of objects in pyramid structure with the following properties:

id , name, value, parentid


Example (C#):


```
#!c#

 var b = new block();
 b.id = 100; 
 b.name = "block 100";
 b.value = 102.50;
 b.parentid = 99;
```

 
Write a recursive function that accepts an ID as the only parameter and will loop through an array or list of an undetermined size and number of levels. The recursive function will calculate the sum of the value of all the blocks that are in the hierarchy under the ID passed to the function.



**Problem 2** 

With the same scenario as in problem 1, write a non-recursive routine that also accepts an ID, and returns a list of id's of all the blocks that are in the hierarchy under the ID passed to the function.

-------------------


# Result #

-------------------

**for Id = 1** 

-------------------

![Result-for-Id-1.jpg](https://bitbucket.org/repo/oLqgpEq/images/4228424029-Result-for-Id-1.jpg)

-------------------

**for Id = 2** 

-------------------

![Result-for-Id-2.jpg](https://bitbucket.org/repo/oLqgpEq/images/2879595677-Result-for-Id-2.jpg)

-------------------

**for Id = 9** 

-------------------


![Result-for-Id-9.jpg](https://bitbucket.org/repo/oLqgpEq/images/758227376-Result-for-Id-9.jpg)

-------------------

**for Id = null** 

-------------------

![Result-for-Id-null.jpg](https://bitbucket.org/repo/oLqgpEq/images/2600457864-Result-for-Id-null.jpg)