﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PyramidTest
{
    class Worker
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? ParentId { get; set; }
        public int Value { get; set; }
        public static List<Worker> Workers;



        /* Task #1
         * Write a recursive function that accepts an ID as the only parameter
         * and will loop through an array or list of an undetermined size and number of levels. 
         * The recursive function will calculate the sum of the value of all the blocks that are 
         * in the hierarchy under the ID passed to the function.
         */             
        public static int SumUnderId(int? id)
        {
            var sum = 0;
            var ids = Workers.Where(w => w.ParentId == id).ToList();
            if (ids.Count() > 0)            
            {
                sum += ids.Sum(v => v.Value);
                foreach (var el in ids)
                {                   
                    sum +=  SumUnderId(el.Id);
                    
                }              
            }
            return sum;        
        }



        /* Task #2
         * With the same scenario as in problem 1, write a non-recursive routine that also accepts
         * an ID, and returns a list of id's of all the blocks that are in the hierarchy under the ID 
         * passed to the function.
         */
        public static List<Worker> IdsUnderId(int? id)
        {
            List<Worker> result = new List<Worker>();
            var ids = Workers.Where(w => w.ParentId == id).ToList();
           
            while (ids.Count() > 0)
            {
                result.AddRange(ids);
                ids = Workers.Where(w => ids.Any(ww => ww.Id == w.ParentId)).ToList();
            }
            Console.WriteLine("-------------------");
            Console.WriteLine("Under Id={0} ", id == null? "null" : id.ToString());
            Console.WriteLine("Ids: {0} ", String.Join(", ", result.Select(r => r.Id).ToArray()));
            return result; 
        }




         /*
         *Create pyramid data
         */
        public static IEnumerable<Worker> CreateWorkers(int levels)
        {
            List<Worker> workers = new List<Worker>();
            Random rnd = new Random();
 
            var id = 1;
            Worker worker;

            List<int> childrenIds = new List<int>();
            List<int> parentIds = new List<int>();
            
            // levels
            for (int level = 1; level <= levels; level++)
            {
                Console.WriteLine("-------------------");
                Console.WriteLine("Level={0} Ids:", level);
                
                if (parentIds.Count() == 0)
                {                  
                    worker = new Worker()
                     {
                         Id = id,
                         Name = "blog " + id,
                         Value =1,
                         ParentId = null
                     };
                    workers.Add(worker);
                    childrenIds.Add(id++);
                    Console.Write("null-{0} ", worker.Id);
                    Console.WriteLine();
                }
                else
                {
                    // parents
                    foreach (var prId in parentIds)
                    {                      
                        var childrenCount = rnd.Next(2, 5);
                        // children for parent
                        for (int chId = 1; chId <= childrenCount; chId++)
                        {
                            worker = new Worker()
                             {
                                 Id = id,
                                 Name = "blog " + id,
                                 Value = id,   //for easy testing value = id
                                 ParentId = prId
                             };
                            workers.Add(worker);
                            childrenIds.Add(id++);
                            Console.Write("{0}-{1} ", prId, worker.Id);

                        }
                        Console.WriteLine();
                    }

                    Console.WriteLine();
                 } 
                
                parentIds = childrenIds;
                childrenIds = new List<int>();
            }
            return workers;        
        }
 
    }
}
