﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PyramidTest
{
    class Program
    {
        static void Main(string[] args)
        {
            
            //create pyramid data 
            Worker.Workers = Worker.CreateWorkers(4).ToList();
            int? testId = 2;

            // task #2
            var idsUnderId = Worker.IdsUnderId(testId);

            //task #1
            var sum = Worker.SumUnderId(testId);
            Console.WriteLine("-------------------");
            Console.WriteLine("Under Id={0} ", testId == null ? "null" : testId.ToString());
            Console.WriteLine("Ids summary ={0}", sum);
            Console.WriteLine("-------------------");
            Console.ReadKey();

        }
    }
}
